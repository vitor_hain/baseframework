package com.letaris.baseframework.data;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

/** Created by Letaris on 1/25/17. */
public class BaseMetaDataUtils
{
	private static Bundle getMetaBundle(Context context)
	{
		try
		{
			ApplicationInfo info = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
			return info.metaData;
		}
		catch (PackageManager.NameNotFoundException e)
		{
			e.printStackTrace();
		}
		return Bundle.EMPTY;
	}

	protected static String getString(Context context, String param)
	{
		Bundle meta = getMetaBundle(context);
		return meta.getString(param);
	}
}


/*

*** Exemplo de como seria uma classe que extende a BaseMetaDataUtils ***

public class PrivacyMetaDataUtils extends BaseMetaDataUtils
{
	private static final String META_TAG_XYZ_AUTHORITY = "xyz.sdk.authority";
	private static final String META_TAG_XYZ_THEME_AUTHORITY = "xyz.theme.sdk.authority";
	private static final String META_TAG_ABC_AUTHORITY = "abc.sdk.authority";

	public static String getXyzAuthority(Context context)
	{
		return getString(context, META_TAG_XYZ_AUTHORITY);
	}

	public static String getXyzThemeAuthority(Context context)
	{
		return getString(context, META_TAG_XYZ_THEME_AUTHORITY);
	}

	public static String getAbcAuthority(Context context)
	{
		return getString(context, META_TAG_ABC_AUTHORITY);
	}
}
*/