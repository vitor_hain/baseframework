package com.letaris.baseframework.fragments;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.PopupMenu;
import android.view.MenuInflater;
import android.view.View;

import com.letaris.baseframework.R;
import com.letaris.baseframework.activities.BaseActivity;
import com.letaris.baseframework.fragments.animation.FragmentTransition;

/** Created by Letaris on 10/14/16. */
public abstract class BaseFragment extends Fragment
{
	protected static final String SAVED_INSTANCE = "saved_instance";

	protected Context mContext;

	/** Concrete methods */
	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);
		mContext = context;
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		if (outState == null)
			outState = new Bundle();

		outState.putBoolean(SAVED_INSTANCE, true);

		super.onSaveInstanceState(outState);
	}

	protected void switchToFragment(Fragment frag, int containerId, boolean addToBackStack)
	{
		switchToFragment(frag, containerId, addToBackStack, FragmentTransition.NONE);
	}

	protected void switchToFragment(Fragment frag, int containerId, boolean addToBackStack, FragmentTransition anim)
	{
		if (isActivityValid())
		{
			final FragmentManager fm = getActivity().getSupportFragmentManager();
			Fragment fragment = fm.findFragmentById(containerId);

			FragmentTransaction ft = fm.beginTransaction();

			/* Only add to backstack if it's not the first fragment */
			if (fragment != null && addToBackStack)
				ft.addToBackStack(frag.getClass().getName());

			if (anim != FragmentTransition.NONE)
				ft.setCustomAnimations(anim.getEnterAnimId(), anim.getExitAnimId(), anim.getPopExitAnimId(), anim.getPopEnterAnimId());

			if (fragment == null)
			{
				ft.add(containerId, frag);
				ft.commitAllowingStateLoss();
			}
			else
			{
				ft.replace(containerId, frag);
				ft.commitAllowingStateLoss();
			}
		}
	}

	protected Fragment getCurrentFragment(int containerId)
	{
		FragmentManager fm = getFragmentManager();
		return fm.findFragmentById(containerId);
	}

	public void dismissDialogWithTag(String tag)
	{
		FragmentManager fm = getFragmentManager();

		if (fm != null)
		{
			Fragment fragment = fm.findFragmentByTag(tag);

			if (fragment != null && fragment instanceof DialogFragment && fragment.isAdded())
				((DialogFragment) fragment).dismissAllowingStateLoss();
		}
	}

	protected void setActionBarElevation(float dp)
	{
		if (isBaseActivity())
		{
			ActionBar actionBar = ((BaseActivity) getActivity()).getSupportActionBar();

			if (actionBar != null)
				actionBar.setElevation(dp);
		}
	}

	protected void setActionBarTitle(int titleResId)
	{
		setActionBarTitle(getString(titleResId));
	}

	protected void setActionBarTitle(String title)
	{
		if (isBaseActivity())
		{
			ActionBar actionBar = ((BaseActivity) getActivity()).getSupportActionBar();

			if (actionBar != null)
				actionBar.setTitle(title);
		}
	}

	protected void setActionBarColor(int resId)
	{
		ActionBar actionBar = ((BaseActivity) getActivity()).getSupportActionBar();

		if (actionBar != null)
		{
			actionBar.setBackgroundDrawable(new ColorDrawable(getContext().getResources().getColor(resId)));
			actionBar.setDisplayShowTitleEnabled(false);
			actionBar.setDisplayShowTitleEnabled(true);
		}
	}

	protected void setActionBarVisibility(boolean visibility)
	{
		if (isBaseActivity())
			((BaseActivity) getActivity()).setActionBarVisible(visibility);
	}

	protected void showMenu(int anchorId, int menuId, PopupMenu.OnMenuItemClickListener listener)
	{
		View anchor = getActivity().findViewById(anchorId);

		Context wrapper = new android.view.ContextThemeWrapper(getActivity(), getPopupMenuStyle());

		PopupMenu menu = new PopupMenu(wrapper, anchor);
		menu.setOnMenuItemClickListener(listener);

		MenuInflater inflater = menu.getMenuInflater();
		inflater.inflate(menuId, menu.getMenu());

		menu.show();
	}

	/** Override this if another style is needed */
	protected int getPopupMenuStyle()
	{
		return R.style.BasePopupMenu;
	}

	public boolean isActivityValid()
	{
		return getActivity() != null && !getActivity().isFinishing();
	}

	protected boolean isBaseActivity()
	{
		return (isActivityValid() && getActivity() instanceof BaseActivity);
	}

	protected boolean hasArgument(String argName)
	{
		return getArguments() != null && getArguments().containsKey(argName);
	}
}