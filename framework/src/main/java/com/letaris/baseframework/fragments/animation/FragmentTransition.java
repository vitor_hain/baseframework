package com.letaris.baseframework.fragments.animation;

import com.letaris.baseframework.R;

/** Created by Letaris on 10/14/16. */
public enum FragmentTransition
{
	RIGHT_IN_RIGHT_OUT(R.anim.fragment_slide_in_left, R.anim.fragment_slide_out_left, R.anim.fragment_slide_out_right, R.anim.fragment_slide_in_right),
	LEFT_IN_LEFT_OUT(R.anim.fragment_slide_out_right, R.anim.fragment_slide_in_right, R.anim.fragment_slide_in_left, R.anim.fragment_slide_out_left),
	NONE(-1, -1, -1, -1);

	FragmentTransition(int enterAnimId, int exitAnimId, int popExitAnimId, int popEnterAnimId)
	{
		mEnterAnimId = enterAnimId;
		mExitAnimId = exitAnimId;
		mPopExitAnimId = popExitAnimId;
		mPopEnterAnimId = popEnterAnimId;
	}

	private final int mEnterAnimId;
	private final int mExitAnimId;
	private final int mPopExitAnimId;
	private final int mPopEnterAnimId;

	public int getEnterAnimId()
	{
		return mEnterAnimId;
	}

	public int getExitAnimId()
	{
		return mExitAnimId;
	}

	public int getPopExitAnimId()
	{
		return mPopExitAnimId;
	}

	public int getPopEnterAnimId()
	{
		return mPopEnterAnimId;
	}
}