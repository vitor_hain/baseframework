package com.letaris.baseframework.activities;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

/** Created by Letaris on 10/10/16. */
public abstract class BaseActivity extends AppCompatActivity
{
	private final String TAG = BaseActivity.class.getSimpleName();

	protected boolean mStateSaved = false;

	/** Abstract methods */
	protected abstract int getLayout();
	protected abstract void actionBarBackPressed();

	/** Concrete methods */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(getLayout());
	}

	protected void setupCustomActionBar(int customLayout)
	{
		ActionBar actionBar = getSupportActionBar();

		if (actionBar != null)
		{
			actionBar.setDisplayShowCustomEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
			actionBar.setCustomView(customLayout);
		}
	}


	public void setActionBarVisible(boolean visible)
	{
		ActionBar actionBar = getSupportActionBar();

		if (actionBar != null)
		{
			if (visible)
				actionBar.show();
			else
				actionBar.hide();
		}
	}

	protected void setActionBarTitle(int textResId)
	{
		ActionBar actionBar = getSupportActionBar();

		if (actionBar != null)
			actionBar.setTitle(textResId);
	}

	protected void setActionBarTitle(String text)
	{
		ActionBar actionBar = getSupportActionBar();

		if (actionBar != null)
			actionBar.setTitle(text);
	}

	protected void addFragment(Fragment frag, Bundle arguments, int container, boolean addToBackStack)
	{
		final FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(container);

		FragmentTransaction ft = fm.beginTransaction();

		if (addToBackStack)
			ft.addToBackStack(frag.getClass().getName());

		if (isFinishing())
			return;

		frag.setArguments(arguments);

		if (fragment == null || addToBackStack)
		{
			ft.add(container, frag, frag.getClass().getName());
			ft.commitAllowingStateLoss();
		}
		else
		{
			ft.replace(container, frag, frag.getClass().getName());
			ft.commitAllowingStateLoss();
		}
	}

	protected void addFragment(Fragment frag, int container, boolean addToBackStack) {
		addFragment(frag, new Bundle(), container, addToBackStack);
	}

	protected boolean hasExtras()
	{
		return getIntent().getExtras() != null;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		mStateSaved = true;
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onResumeFragments()
	{
		super.onResumeFragments();
		mStateSaved = false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				actionBarBackPressed();
				try
				{
					onBackPressed();
				}
				catch (Exception e)
				{
					Log.e(TAG, "Child class: " + this.getClass().getSimpleName(), e);
					throw e;
				}
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public boolean isStateSaved()
	{
		return mStateSaved;
	}

	protected Fragment getCurrentFragment(int containerId)
	{
		FragmentManager fm = getSupportFragmentManager();
		return fm.findFragmentById(containerId);
	}

	protected void clearBackStack()
	{
		FragmentManager manager = getSupportFragmentManager();

		while (manager.getBackStackEntryCount() > 0)
			manager.popBackStackImmediate();
	}

	protected void dismissDialogWithTag(String tag)
	{
		Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);

		if (fragment != null && fragment instanceof DialogFragment && fragment.isAdded())
			((DialogFragment) fragment).dismissAllowingStateLoss();
	}

	/** Consume Menu Button event */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		return (keyCode == KeyEvent.KEYCODE_MENU || super.onKeyUp(keyCode, event));
	}
}