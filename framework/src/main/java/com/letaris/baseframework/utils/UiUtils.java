package com.letaris.baseframework.utils;

import android.content.Context;

/**
 * Created by vitor.hain on 2/27/18.
 */

public class UiUtils
{
    public static int dpToPx(Context context, float dpValue)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return Math.round(dpValue * scale);
    }

    public static int pxToDp(Context context, float pxValue)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return Math.round(pxValue / scale);
    }
}