package com.letaris.baseframework.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/** Created by Letaris on 1/25/17. */

public abstract class BaseOpenHelper extends SQLiteOpenHelper
{
	protected static final String EMPTY_CHAR = " ";
	protected Context mContext;

	public BaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
	{
		super(context, name, factory, version);
		mContext = context.getApplicationContext();
	}

	protected String genCreateRequest(String table, String[] columns, String[] types)
	{
		return genCreateRequest(table, columns, types, new String[]{});
	}

	protected String genCreateRequest(String table, String[] columns, String[] types, String[] primaryKeys)
	{
		StringBuilder stringBuilder = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
		stringBuilder.append(table).append('(');
		for (int i = 0; i < columns.length; i++)
		{
			stringBuilder.append(columns[i]).append(' ').append(types[i]);

			if (i < (columns.length - 1))
				stringBuilder.append(',');
		}

		if (primaryKeys.length > 0)
		{
			stringBuilder.append(", PRIMARY KEY (");
			for (int i = 0; i < primaryKeys.length; i++)
			{
				stringBuilder.append(primaryKeys[i]);

				if (i < (columns.length - 1))
					stringBuilder.append(", ");
				else
					stringBuilder.append(")");
			}
		}

		stringBuilder.append(");");
		return stringBuilder.toString();
	}

	protected String genAddColumnRequest(String table, String column, String type)
	{
		String str = "ALTER TABLE ";
		str = str.concat(table).concat(EMPTY_CHAR);
		str = str.concat("ADD COLUMN ");
		str = str.concat(column).concat(EMPTY_CHAR);
		str = str.concat(type).concat(";");

		return str;
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVer, int newVer)
	{
		if (oldVer == newVer)
			return;

		onUpgrade(sqLiteDatabase, oldVer, newVer - 1);
		upgradeToVersion(sqLiteDatabase, newVer);
	}

	protected abstract void upgradeToVersion(SQLiteDatabase db, int curUpgrade);
}