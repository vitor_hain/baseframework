package com.letaris.baseframework.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/** Created by Letaris on 1/25/17. */
public abstract class BaseContentProvider extends ContentProvider
{
	private static final String TAG = BaseContentProvider.class.getSimpleName();

	protected UriMatcher mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	protected BaseOpenHelper mHelper;
	protected SQLiteDatabase mSqLiteDb = null;

	protected String mAuthority;

	@Override
	public abstract boolean onCreate();

	protected abstract void init();

	protected synchronized SQLiteDatabase getSqLiteDb() throws Exception
	{
		if (mSqLiteDb == null)
			mSqLiteDb = mHelper.getWritableDatabase();

		return mSqLiteDb;
	}

	@Nullable
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String order)
	{
		Cursor cursor = null;

		try
		{
			SQLiteDatabase db = getSqLiteDb();
			cursor = queryDb(db, uri, projection, selection, selectionArgs, order);

			if (cursor != null)
				setNotificationUri(cursor, uri);

		}
		catch (Exception error)
		{
			Log.e(TAG, "", error);
		}

		return cursor;
	}

	protected abstract Cursor queryDb(SQLiteDatabase db, Uri uri, String[] projection, String selection, String[] selectionArgs, String order);

	@Nullable
	@Override
	public String getType(Uri uri)
	{
		return null;
	}

	@Nullable
	@Override
	public Uri insert(Uri uri, ContentValues values)
	{
		Uri ret = null;

		try
		{
			SQLiteDatabase db = getSqLiteDb();
			ret = insertOnDb(db, uri, values);

			notifyChange(uri, null);
		}
		catch (Exception error)
		{
			Log.e(TAG, "", error);
		}

		return ret;
	}

	protected abstract Uri insertOnDb(SQLiteDatabase db, Uri uri, ContentValues contentValues);

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
	{
		int count = 0;

		try
		{
			SQLiteDatabase db = getSqLiteDb();
			count = updateDb(db, uri, values, selection, selectionArgs);

			notifyChange(uri, null);
		}
		catch (Exception error)
		{
			Log.e(TAG, "", error);
		}

		return count;
	}

	protected abstract int updateDb(SQLiteDatabase db, Uri uri, ContentValues contentValues, String selection, String[] selectionArgs);

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs)
	{
		int count = 0;

		try
		{
			SQLiteDatabase db = getSqLiteDb();
			count = deleteFromDb(db, uri, selection, selectionArgs);

			notifyChange(uri, null);
		}
		catch (Exception error)
		{
			Log.e(TAG, "", error);
		}

		return count;
	}

	protected abstract int deleteFromDb(SQLiteDatabase db, Uri uri, String selection, String[] selectionArgs);

	protected void setNotificationUri(Cursor cursor, Uri uri)
	{
		Context c = getContext();

		if (c != null)
		{
			ContentResolver resolver = c.getContentResolver();
			cursor.setNotificationUri(resolver, uri);
		}
	}

	protected void notifyChange(Uri uri, ContentObserver observer)
	{
		Context c = getContext();

		if (c != null)
		{
			ContentResolver resolver = c.getContentResolver();
			resolver.notifyChange(uri, observer);
		}
	}
}